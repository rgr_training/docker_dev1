FROM node:8.5-alpine

LABEL maintainer="Richard Guitter <rguitter@gmail.com>"
LABEL version="1-SNAPSHOT" \
      description="A one day training for docker targeting developper (level 1)"

COPY . .

#RUN mkdir node_modules
#COPY node_modules node_modules
RUN npm install
#RUN npm prune --production

EXPOSE 8000

ENTRYPOINT [ "npm" ]
CMD [ "start" ]