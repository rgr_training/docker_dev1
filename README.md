# Training

Subject: Docker for developer (getting started)

# Slide deck

## Writing

The slides are done with [revealjs](https://github.com/hakimel/reveal.js/)
It's done by simply cloning the revealjs repository.

To start authoring some content just run `npm start` and go to [htpp://localhost:8000] in order display the result.
Then simply edit the files and you will see the display being updated on the fly.

## Packaging

### Simple html/js

Just run `npm build`.

### A dockerize version

Once done it's possible build the slide deck as a docker image by running:
`docker build -t tr-docker-lvl1 .`
`docker run -d -p 8000:8000 --name tr-docker-lvl1 tr-docker-lvl1`

## Deploy

### As a gitlab page

Highigh of the main seciton in the `.gitlab-ci`:

```
pages:
  stage: deploy
  dependencies:
    - build
  script: 
    - mkdir .public
    - cp -r * .public
    - mv .public public
  artifacts:
    paths:
      - public/
  only:
    - master
    - fix_missing_images
```

The slides are then available on [http://rgr_training.gitlab.io/docker_dev1/](http://rgr_training.gitlab.io/docker_dev1/).

### As a S3 static web site

This is inpire by this [post](https://about.gitlab.com/2016/08/26/ci-deployment-and-environments/)

The slides are then available on [http://rgr-training-docker1.s3-website.eu-central-1.amazonaws.com/](http://rgr-training-docker1.s3-website.eu-central-1.amazonaws.com/)

